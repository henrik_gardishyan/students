package henrik.gardishyan.students.dialogs

/**
 * UI dialog for student add/edit operations
 */

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import henrik.gardishyan.students.R
import henrik.gardishyan.students.provider.Student
import henrik.gardishyan.students.provider.StudentsViewModel
import henrik.gardishyan.students.utils.StudentsSnackBar
import java.text.SimpleDateFormat
import java.util.*

private const val ARG_GID = "arg_gid"
private const val ARG_STUDENT = "arg_student"

class StudentDialog() : BottomSheetDialogFragment() {
    private val studentsViewModel: StudentsViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val ret = super.onCreateDialog(savedInstanceState)
        (ret as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
        return ret
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        val root = inflater.inflate(R.layout.dialog_student, container, false)
        val root = (DataBindingUtil.inflate(inflater, R.layout.dialog_student, container, false) as ViewDataBinding).root

        val title = root.findViewById<TextView>(R.id.student_add_title)
        val first = root.findViewById<EditText>(R.id.student_first)
        val middle = root.findViewById<EditText>(R.id.student_middle)
        val last = root.findViewById<EditText>(R.id.student_last)
        val birthday = root.findViewById<DatePicker>(R.id.student_birthday_picker)
        val gid = arguments?.getLong(ARG_GID)?:0
        val ss = arguments?.getSerializable(ARG_STUDENT)
        var sid: Long? = null
        if(ss != null) {
            title.text = getString(R.string.label_student_edit)
            val student = ss as Student
            sid = student.id
            first.setText(student.first)
            middle.setText(student.middle)
            last.setText(student.last)
            val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.ROOT)
            val cld = Calendar.getInstance()
            cld.time = sdf.parse(student.birthday)
            birthday.updateDate(cld.get(Calendar.YEAR), cld.get(Calendar.MONTH), cld.get(Calendar.DAY_OF_MONTH))
        }

        val submit = root.findViewById<TextView>(R.id.student_submit)
        submit.setOnClickListener {
            if (sid != null) {
                if (!studentsViewModel.updateStudent(
                        gid, Student(
                            sid, first.text.toString(),
                            middle.text.toString(), last.text.toString(),
                            "${birthday.dayOfMonth}-${birthday.month}-${birthday.year}"
                        )
                    )
                ) {
                    StudentsSnackBar.show(
                        requireActivity(),
                        getString(R.string.msg_err_update_student),
                        false
                    )
                }
            } else if (!studentsViewModel.createStudent(
                    gid, first.text.toString(),
                    middle.text.toString(), last.text.toString(),
                    "${birthday.dayOfMonth}-${birthday.month}-${birthday.year}"
                )
            ) {
                StudentsSnackBar.show(
                    requireActivity(),
                    getString(R.string.msg_err_add_student),
                    false
                )
            }
            dismiss()
        }

        return root
    }

    companion object {
        fun newInstance(gid: Long, student: Student?): StudentDialog =
            StudentDialog().apply {
                arguments = Bundle().apply {
                    putLong(ARG_GID, gid)
                    putSerializable(ARG_STUDENT, student)
                }
            }
    }
}
