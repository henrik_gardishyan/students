package henrik.gardishyan.students.dialogs

/**
 * UI dialog for group add/edit operations
 */

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import henrik.gardishyan.students.provider.Group
import henrik.gardishyan.students.R
import henrik.gardishyan.students.provider.StudentsViewModel
import henrik.gardishyan.students.utils.StudentsSnackBar

private const val ARG_GROUP = "arg_group"

class GroupDialog() : BottomSheetDialogFragment() {
    private val studentsViewModel: StudentsViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val ret = super.onCreateDialog(savedInstanceState)
        (ret as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
        return ret
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        val root = inflater.inflate(R.layout.dialog_group, container, false)
        val root = (DataBindingUtil.inflate(inflater, R.layout.dialog_group, container, false) as ViewDataBinding).root

        val title = root.findViewById<TextView>(R.id.group_add_title)
        val groupName = root.findViewById<EditText>(R.id.group_name)
        val groupFaculty = root.findViewById<EditText>(R.id.group_faculty)
        val gg = arguments?.getSerializable(ARG_GROUP)
        var gid: Long? = null
        if(gg != null) {
            title.text = getString(R.string.label_group_edit)
            val group = gg as Group
            gid = group.id
            groupName.setText(group.name)
            groupFaculty.setText(group.faculty)
        }

        val submit = root.findViewById<TextView>(R.id.group_submit)
        submit.setOnClickListener {
            if(gid != null) {
                if(!studentsViewModel.updateGroup(Group(gid, groupName.text.toString(), groupFaculty.text.toString()))) {
                    StudentsSnackBar.show(requireActivity(), getString(R.string.msg_err_update_group), false)
                }
            } else if(!studentsViewModel.createGroup(groupName.text.toString(), groupFaculty.text.toString())) {
                StudentsSnackBar.show(requireActivity(), getString(R.string.msg_err_add_group), false)
            }
            dismiss()
        }
        return root
    }

    companion object {
        fun newInstance(group: Group?): GroupDialog =
            GroupDialog().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_GROUP, group)
                }
            }
    }
}
