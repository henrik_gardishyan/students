package henrik.gardishyan.students

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import henrik.gardishyan.students.dialogs.GroupDialog
import henrik.gardishyan.students.dialogs.StudentDialog
import henrik.gardishyan.students.provider.All
import henrik.gardishyan.students.provider.Student
import henrik.gardishyan.students.provider.StudentsViewModel
import henrik.gardishyan.students.utils.ContextDialog
import henrik.gardishyan.students.utils.StudentsSnackBar
import henrik.gardishyan.students.utils.TopToolbar

/**
 * The main view of app, implements collapsible list of groups and students.
 * Data provider based on MVVM pattern
 */

class MainActivity : AppCompatActivity() {
    private val studentsViewModel: StudentsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listAdapter = MainListAdapter()
        val mainList = findViewById<RecyclerView>(R.id.group_list)
        mainList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mainList.adapter = listAdapter

        val topToolbar = findViewById<TopToolbar>(R.id.top_toolbar)
        topToolbar.addListener = {
            GroupDialog.newInstance(null).show(supportFragmentManager, "group_picker")
        }
        topToolbar.searchListener = {
            studentsViewModel.updateSearch(it)
        }

        val swipeRefresh = findViewById<SwipeRefreshLayout>(R.id.swipe_refresh)
        swipeRefresh.setOnRefreshListener {
            listAdapter.collapseAll()
            studentsViewModel.updateData()
        }

        swipeRefresh.isRefreshing = true
        val emptyView = findViewById<TextView>(R.id.empty_view)
        studentsViewModel.all.observe(this) { groups ->
            mainList.visibility = if(groups.size > 0) View.VISIBLE else View.INVISIBLE
            emptyView.visibility = if(groups.size > 0) View.INVISIBLE else View.VISIBLE
            listAdapter.update(groups)
            swipeRefresh.isRefreshing = false
        }
    }

    private inner class MainListAdapter() : RecyclerView.Adapter<MainListAdapter.ViewHolder>() {
        private var all = ArrayList<All>()
        private var expanded = HashMap<Long, Boolean>()

        fun update(nd: ArrayList<All>) {
            all = nd.clone() as ArrayList<All>
            notifyDataSetChanged()
        }

        fun collapseAll() {
            expanded.clear()
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.groupName.text = all[position].group.name
            holder.groupFaculty.text = all[position].group.faculty
            holder.groupStudents.text = "${all[position].students.size}"
            if(all[position].students.size > 0) {
                holder.groupMenu.setImageResource(R.drawable.ic_plus)
            } else {
                holder.groupMenu.setImageResource(R.drawable.ic_dots)
            }
            holder.studentListAdapter.update(all[position].group.id, all[position].students)
            if(expanded[all[position].group.id]==true) {
                holder.groupStudentsList.visibility = View.VISIBLE
                holder.groupCollapse.setImageResource(R.drawable.ic_arrow_down)
            } else {
                holder.groupStudentsList.visibility = View.GONE
                holder.groupCollapse.setImageResource(R.drawable.ic_arrow_right)
            }
        }

        override fun getItemCount(): Int {
            return all.size
        }

        private inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup)
            : RecyclerView.ViewHolder(inflater.inflate(R.layout.view_group_item, parent, false)) {

            val groupCollapse = itemView.findViewById<ImageView>(R.id.group_collapse)
            val groupName = itemView.findViewById<TextView>(R.id.group_name)
            val groupFaculty = itemView.findViewById<TextView>(R.id.group_faculty)
            val groupStudents = itemView.findViewById<TextView>(R.id.group_students)
            val groupStudentsList = itemView.findViewById<RecyclerView>(R.id.group_students_list)
            val groupMenu = itemView.findViewById<ImageView>(R.id.group_menu)
            val groupContent = itemView.findViewById<ConstraintLayout>(R.id.group_item_content)
            val studentListAdapter = StudentListAdapter()

            init {
                groupContent.setOnClickListener {
                    GroupDialog.newInstance(all[absoluteAdapterPosition].group).show(supportFragmentManager, "group_picker")
                }
                groupMenu.setOnClickListener {
                    if(all[absoluteAdapterPosition].students.size > 0) {
                        StudentDialog.newInstance(all[absoluteAdapterPosition].group.id, null).show(supportFragmentManager, "student_picker")
                    } else {
                        ContextDialog(R.menu.group_menu, it) { mid ->
                            when(mid) {
                                R.id.action_add -> StudentDialog.newInstance(all[absoluteAdapterPosition].group.id, null).show(supportFragmentManager, "student_picker")
                                R.id.action_delete -> {
                                    if(!studentsViewModel.deleteGroup(all[absoluteAdapterPosition].group.id)) {
                                        StudentsSnackBar.show(this@MainActivity, getString(R.string.msg_err_delete_group), false)
                                    }
                                }
                            }
                        }.show(supportFragmentManager, "group_context_menu")
                    }
                }
                groupCollapse.setOnClickListener {
                    expanded[all[absoluteAdapterPosition].group.id] = !(expanded[all[absoluteAdapterPosition].group.id]?:false)
                    notifyDataSetChanged()
                }
                groupStudentsList.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
                groupStudentsList.adapter = studentListAdapter
            }
        }
    }

    private inner class StudentListAdapter() : RecyclerView.Adapter<StudentListAdapter.ViewHolder>() {
        private var gid: Long = 0
        private var students = ArrayList<Student>()

        fun update(gd: Long, nd: ArrayList<Student>) {
            gid = gd
            students = nd.clone() as ArrayList<Student>
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.first.text = students[position].first
            holder.middle.text = students[position].middle
            holder.last.text = students[position].last
            holder.birthday.text = students[position].birthday
        }

        override fun getItemCount(): Int {
            return students.size
        }

        private inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup)
            : RecyclerView.ViewHolder(inflater.inflate(R.layout.view_student_item, parent, false)) {

            val first: TextView = itemView.findViewById(R.id.student_first)
            val middle: TextView = itemView.findViewById(R.id.student_middle)
            val last: TextView = itemView.findViewById(R.id.student_last)
            val birthday: TextView = itemView.findViewById(R.id.student_birthday)
            val studentContent: ConstraintLayout = itemView.findViewById(R.id.student_item_content)
            val studentDelete: ImageView = itemView.findViewById(R.id.student_delete)

            init {
                studentContent.setOnClickListener {
                    StudentDialog.newInstance(gid, students[absoluteAdapterPosition]).show(supportFragmentManager, "student_picker")
                }
                studentDelete.setOnClickListener {
                    if(!studentsViewModel.deleteStudent(students[absoluteAdapterPosition].id)) {
                        StudentsSnackBar.show(this@MainActivity, getString(R.string.msg_err_delete_student), false)
                    }
                }
            }
        }
    }
}
