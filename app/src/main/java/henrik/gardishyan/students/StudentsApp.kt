package henrik.gardishyan.students

import android.app.Application
import com.google.firebase.FirebaseApp
import henrik.gardishyan.students.utils.Blog

/**
 * Custom app class, provides application context
 */

class StudentsApp : Application() {

    companion object {
        private lateinit var _instance: StudentsApp
        fun get(): StudentsApp {
            return _instance
        }
    }

    init {
        _instance = this
    }
}
