package henrik.gardishyan.students.utils

/**
 * Custom top toolbar with support of search view
 */

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import henrik.gardishyan.students.R

class TopToolbar(context: Context, attrs: AttributeSet?) : Toolbar(context, attrs) {
    var searchListener: ((v: String) -> Unit)? = null
    var addListener: (() -> Unit)? = null

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v: View = inflater.inflate(R.layout.view_top_toolbar, this, true)
        setContentInsetsAbsolute(0, 0)
        val a = context.obtainStyledAttributes(attrs, R.styleable.TopToolbar, 0, 0)
        val title = v.findViewById<TextView>(R.id.toolbar_title)
        title.text = a.getString(R.styleable.TopToolbar_title)

        val left = a.getString(R.styleable.TopToolbar_leftMode)
        if(left==context.getString(R.string.toolbar_mode_search)) {
            val sv = v.findViewById<SearchView>(R.id.toolbar_search_left)
            sv.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String?): Boolean {
                    if (newText != null) {
                        searchListener?.let { it(newText) }
                    }
                    return true
                }

                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }
            })
            sv.visibility = VISIBLE
        } else {
            val nv = v.findViewById<View>(R.id.toolbar_left_none)
            nv.visibility = VISIBLE
        }

        val right = a.getString(R.styleable.TopToolbar_rightMode)
        if(right==context.getString(R.string.toolbar_mode_add)) {
            val av = v.findViewById<ImageView>(R.id.toolbar_add)
            av.setOnClickListener{
                addListener?.let { it1 -> it1() }
            }
            av.visibility = VISIBLE
        } else {
            val nv = v.findViewById<View>(R.id.toolbar_right_none)
            nv.visibility = VISIBLE
        }
        a.recycle()
    }
}
