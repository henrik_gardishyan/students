package henrik.gardishyan.students.provider
/**
 * Handles All (Group + Students) live data CRUD operations
 */

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import henrik.gardishyan.students.*
import kotlinx.coroutines.launch

class StudentsViewModel() : ViewModel() {
    private val databaseHelper: DatabaseHelper = DatabaseHelper(StudentsApp.get())
    val all = MutableLiveData<ArrayList<All>>()
    var searchStr: String = ""

    init {
        updateData()
    }

    fun createGroup(n: String, f: String): Boolean {
        val ret = databaseHelper.createGroup(n, f)
        updateData()
        return ret
    }

    fun updateGroup(group: Group): Boolean {
        val ret = databaseHelper.updateGroup(group)
        updateData()
        return ret
    }

    fun deleteGroup(id: Long): Boolean {
        val ret = databaseHelper.deleteGroup(id)
        updateData()
        return ret
    }

    fun createStudent(gid: Long, f: String, m: String, l: String, b: String): Boolean {
        val ret = databaseHelper.createStudent(gid, f, m, l, b)
        updateData()
        return ret
    }

    fun updateStudent(gid: Long, student: Student): Boolean {
        val ret = databaseHelper.updateStudent(gid, student)
        updateData()
        return ret
    }

    fun deleteStudent(id: Long): Boolean {
        val ret = databaseHelper.deleteStudent(id)
        updateData()
        return ret
    }

    fun updateSearch(str: String) {
        searchStr = str
        updateData()
    }

    fun updateData() {
        viewModelScope.launch {
            all.value = databaseHelper.readAll(searchStr)
        }
    }
}
