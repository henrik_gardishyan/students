package henrik.gardishyan.students.provider

/**
 * Handles local DB CRUD operations
 */

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import henrik.gardishyan.students.STUDENTS_DB_NAME
import henrik.gardishyan.students.STUDENTS_DB_VERSION
import henrik.gardishyan.students.utils.Blog
import kotlinx.coroutines.delay
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

data class Student(
    val id: Long,
    val first: String,
    val middle: String,
    val last: String,
    val birthday: String
): Serializable

data class Group(
    val id: Long,
    val name: String,
    val faculty: String
): Serializable

data class All(
    val group: Group,
    val students: ArrayList<Student>
): Serializable

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, STUDENTS_DB_NAME,
    null, STUDENTS_DB_VERSION
) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(sqlGroups())
        db.execSQL(sqlStudents())
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    private fun now(): String {
        val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
        return formatter.format(Calendar.getInstance().time)
    }

    fun createGroup(n: String, f: String): Boolean {
        try {
            val db = writableDatabase

            val values = ContentValues()
            values.put("name", n)
            values.put("faculty", f)

            val newRowId = db.insert("groups", null, values)
            return newRowId > 0
        } catch (e: SQLiteException) {
            Blog.e("DB createGroup error: $e")
        }
        return false
    }

    fun updateGroup(group: Group): Boolean {
        try {
            val db = writableDatabase

            val values = ContentValues()
            values.put("name", group.name)
            values.put("faculty", group.faculty)

            val rowId = db.update("groups", values, "rowid = ?", arrayOf(group.id.toString()))
            return rowId > 0
        } catch (e: SQLiteException) {
            Blog.e("DB updateGroup error: $e")
        }
        return false
    }

    fun deleteGroup(id: Long): Boolean {
        try {
            val db = writableDatabase
            db.delete("groups", "rowid=?", arrayOf(id.toString()))
            return true
        } catch (e: SQLiteException) {
            Blog.e("DB deleteGroup error: $e")
        }
        return false
    }

    fun createStudent(gid: Long, f: String, m: String, l: String, b: String): Boolean {
        try {
            val db = writableDatabase

            val values = ContentValues()
            values.put("first", f)
            values.put("middle", m)
            values.put("last", l)
            values.put("birthday", b)
            values.put("gid", gid)

            val newRowId = db.insert("students", null, values)
            return newRowId > 0
        } catch (e: SQLiteException) {
            Blog.e("DB createStudent error: $e")
        }
        return false
    }

    fun updateStudent(gid: Long, student: Student): Boolean {
        try {
            val db = writableDatabase

            val values = ContentValues()
            values.put("first", student.first)
            values.put("middle", student.middle)
            values.put("last", student.last)
            values.put("birthday", student.birthday)
            values.put("gid", gid)

            val rowId = db.update("students", values, "rowid LIKE ?", arrayOf(student.id.toString()))
            return rowId > 0
        } catch (e: SQLiteException) {
            Blog.e("DB updateStudent error: $e")
        }
        return false
    }

    fun deleteStudent(id: Long): Boolean {
        try {
            val db = writableDatabase
            db.delete("students", "rowid=?", arrayOf(id.toString()))
            return true
        } catch (e: SQLiteException) {
            Blog.e("DB deleteStudent error: $e")
        }
        return false
    }

    suspend fun readAll(ss: String): ArrayList<All> {
        val all = ArrayList<All>()
        val db = writableDatabase

        delay(800) // heavy processing imitation
        try {
            val cpg = db.rawQuery("SELECT rowid, * FROM groups", null)
            if (cpg.moveToFirst()) {
                while (!cpg.isAfterLast) {
                    val gid = cpg.getLong(cpg.getColumnIndexOrThrow("rowid"))
                    val name = cpg.getString(cpg.getColumnIndexOrThrow("name"))
                    val faculty = cpg.getString(cpg.getColumnIndexOrThrow("faculty"))
                    val students = ArrayList<Student>()

                    val cps = db.rawQuery("SELECT rowid, * FROM students WHERE gid=? AND LOWER(last) LIKE LOWER('%${ss}%')", arrayOf(gid.toString()))
                    if (cps.moveToFirst()) {
                        while (!cps.isAfterLast) {
                            val sid = cps.getLong(cps.getColumnIndexOrThrow("rowid"))
                            val first = cps.getString(cps.getColumnIndexOrThrow("first"))
                            val middle = cps.getString(cps.getColumnIndexOrThrow("middle"))
                            val last = cps.getString(cps.getColumnIndexOrThrow("last"))
                            val birthday = cps.getString(cps.getColumnIndexOrThrow("birthday"))
                            students.add(Student(sid, first, middle, last, birthday))
                            cps.moveToNext()
                        }
                    }
                    all.add(All(Group(gid, name, faculty), students))
                    cps.close()
                    cpg.moveToNext()
                }
            }
            cpg.close()
        } catch (e: SQLiteException) {
            Blog.e("DB readAll error: $e")
        }
        return ArrayList(all.filter { ait ->
                        ait.group.name.contains(ss, true) || ait.students.size > 0
                    })
    }

    private fun sqlGroups(): String {
        return "CREATE TABLE IF NOT EXISTS groups (" +
                "name TEXT," +
                "faculty TEXT" +
                ")"
    }

    private fun sqlStudents(): String {
        return "CREATE TABLE IF NOT EXISTS students (" +
                "first TEXT," +
                "middle TEXT," +
                "last TEXT," +
                "birthday TEXT," +
                "gid INTEGER" +
                ")"
    }
}
